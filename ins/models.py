from otree.api import (
    models, widgets, BaseConstants, BaseSubsession, BaseGroup, BasePlayer,
    Currency as c, currency_range
)
from ins.config import *
from django.utils.translation import ugettext as _
import math
import random
import numpy as np
from scipy.stats import geom, poisson, binom, randint

author = 'Christoph Huber'

doc = """
Instructions for Pay-It-Forward Game
"""


class Subsession(BaseSubsession):

    def creating_session(self):
        if self.round_number == 1:
            self.start_session()

    def start_session(self):

        breaks = [j * Constants.num_prices for j in range(1, Constants.num_repetitions + 1)]
        firstround = [j * Constants.num_prices - Constants.num_prices + 1 for j in range(1, Constants.num_repetitions + 1)]

        n = Constants.num_players
        nprices = Constants.num_prices

        for p in self.get_players():

            p.participant.vars['random_round'] = random.randint(1, Constants.num_repetitions)
            p.random_round = p.participant.vars['random_round']

            # p.participant.vars['breaks'] = breaks
            # p.participant.vars['firstround'] = firstround
            # iround = [round + p.id_in_group - 1 for round in firstround]
            # p.participant.vars['iround'] = iround
            # waitround = []
            # for id in range(1, p.id_in_group+1):
            #     waitround.extend([round + id - 1 for round in firstround])
            # p.participant.vars['waitround'] = waitround

            if self.round_number == 1:
                p.participant.vars['repetition'] = 1

            indices = [i for i in range(1, nprices + 1)]
            form_fields = ['buy_' + str(k) for k in indices]

            p.participant.vars['form_fields'] = form_fields

            p.participant.vars['buy'] = []
            p.participant.vars['buys'] = []

        for group in self.get_groups():

            indices = [i for i in range(1, n + 1)]
            positions = [i for i in range(1, n + 1)]

            # determine initial price depending on cap
            # ----------------------------------------------------------------------------------------------------
            price = 1
            possibleinitprices = [1]
            # while price < Constants.cap:
            #     price = price * Constants.multiple
            #     possibleinitprices += [price]

            price1 = Constants.multiple1
            # if Constants.cap == 0:
            #     if Constants.dist_initprices == 'geometric':
            #         n1 = np.random.geometric(Constants.p)
            #     elif Constants.dist_initprices == 'poisson':
            #         n1 = np.random.poisson(Constants.p)
            #     elif Constants.dist_initprices == 'binomial':
            #         n1 = np.random.binomial(Constants.N, Constants.p)
            #     elif Constants.dist_initprices == 'uniform':
            #         n1 = np.random.randint(0, Constants.N + 1)
            #     price1 = Constants.multiple ** (n1 - 1)
            # else:
            #     while price1 not in possibleinitprices:
            #         if Constants.dist_initprices == 'geometric':
            #             n1 = np.random.geometric(Constants.p)
            #         elif Constants.dist_initprices == 'poisson':
            #             n1 = np.random.poisson(Constants.p)
            #         elif Constants.dist_initprices == 'binomial':
            #             n1 = np.random.binomial(Constants.N, Constants.p)
            #         elif Constants.dist_initprices == 'uniform':
            #             n1 = np.random.randint(0, Constants.N + 1)
            #         price1 = Constants.multiple ** (n1 - 1)

            prices = [price1 * (Constants.multiple ** (i + 1)) for i in positions]
            prices_taker = prices
            prices_others = [price1 * (Constants.multiple ** (i - 1)) for i in positions]


            # random player matching
            # ----------------------------------------------------------------------------------------------------
            players = [group.get_player_by_id(k).id_in_group for k in range(1, n + 1)]
            if Constants.random_grouping:
                random.shuffle(players)

            choices = list(zip(indices, positions, prices, players))

            # determine possible choices
            # ----------------------------------------------------------------------------------------------------
            if Constants.num_prices == Constants.num_players:
                prices_displayed = prices
            else:
                prices_displayed = []
                p1 = price1
                while p1 > 1 and len(prices_displayed) < Constants.num_prices - Constants.num_players:
                    p1 = p1/Constants.multiple
                    prices_displayed = [int(p1)] + prices_displayed
                prices_displayed = prices_displayed + prices
                while len(prices_displayed) < Constants.num_prices:
                    pn = prices_displayed[-1] * Constants.multiple
                    prices_displayed = prices_displayed + [pn]

            prices_displayed_zip = list(zip(range(1, len(prices_displayed) + 1), prices_displayed))

            # store <choices> and <prices_displayed> on each player of each group
            # ----------------------------------------------------------------------------------------------------
            # for j in range(1, Constants.num_players + 1):
            for p in group.get_players():
                # p = group.get_player_by_id(j)
                p.participant.vars['choices'] = choices
                p.participant.vars['prices_displayed'] = prices_displayed_zip
                p.participant.vars['prices_taker'] = prices_taker
                p.participant.vars['prices_others'] = prices_others

                # determine order of prices from <order>
                # ------------------------------------------------------------------------------------------------
                if Constants.order == 'random':
                    random.shuffle(p.participant.vars['prices_displayed'])
                elif Constants.order == 'ascending':
                    p.participant.vars['prices_displayed'] = sorted(p.participant.vars['prices_displayed'],
                                                                    key=lambda x: x[0])
                    p.participant.vars['choices'] = sorted(p.participant.vars['choices'],
                                                           key=lambda x: x[0])
                elif Constants.order == 'descending':
                    p.participant.vars['prices_displayed'] = sorted(p.participant.vars['prices_displayed'],
                                                                    key=lambda x: x[0], reverse=True)
                    p.participant.vars['choices'] = sorted(p.participant.vars['choices'],
                                                           key=lambda x: x[0], reverse=True)

                # generate data for game tree
                #  -----------------------------------------------------------------------------------------------
                GTindices = [a for a, b, c, d in choices]
                GTprices = [c for a, b, c, d in choices]
                GTxpoints = [(40+480/Constants.num_players) * j for j in range(0, len(GTprices))]

                GT = list(zip(GTindices, GTprices, GTxpoints))
                p.participant.vars['GameTree'] = GT

                GTpayoff = []
                for pos in positions:
                    temp = []
                    i = 1
                    while i < pos:
                        temp.extend([Constants.multiple1 * Constants.earnings_success ** (pos - 1)])
                        i = i+1
                    GTpayoff.append(temp)
                    GTpayoff[pos-1].extend([Constants.multiple1 * Constants.earnings_success ** (pos + 1)])
                    GTpayoff[pos-1].extend((Constants.num_players-pos) * [0])
                GTpayoffs = []
                for j in GTpayoff:
                    GTpayoffs.extend([tuple(j)])

                # combinations = [(a, b) for a in range(0, Constants.num_players) for b in range(0, Constants.num_players) if a + b == Constants.num_players - 1]
                # GTpayoff1 = tuple([Constants.earnings_noaction] * Constants.num_players + [0])
                GTpayoff1 = GTpayoffs[0]
                # GTpayoffN = [tuple([Constants.earnings_success] * x + [0] + [Constants.earnings_noaction] * y ) for x, y in combinations][-1]
                GTpayoffN = tuple(Constants.num_players * [int(Constants.multiple1 * Constants.earnings_success ** Constants.num_players)])
                GTpayoffN = [(str(GTpayoffN), GTxpoints[Constants.num_players-1])]
                # GTpayoffs = [tuple([Constants.earnings_success] * x + [0] + [Constants.earnings_noaction] * y) for x, y in combinations][0:(len(combinations)-1)]


                p.participant.vars['GTpayoff1'] = GTpayoff1
                p.participant.vars['GTpayoffN'] = GTpayoffN
                p.participant.vars['GTpayoffs'] = GTpayoffs
                p.participant.vars['GTxpoints'] = GTxpoints
                # p.participant.vars['combinations'] = combinations

                # generate table for instructions
                #  -----------------------------------------------------------------------------------------------
                if Constants.cap == 0:
                    TABprices = [Constants.multiple ** i for i in range(0, Constants.num_instrprices)]
                    if Constants.dist_initprices == 'geometric':
                        TABprobabilities = [geom.pmf(j, Constants.p) * 100 for j in range(1, len(TABprices) + 1)]
                    elif Constants.dist_initprices == 'poisson':
                        TABprobabilities = [poisson.pmf(j, Constants.p) * 100 for j in range(0, len(TABprices))]
                    elif Constants.dist_initprices == 'binomial':
                        TABprobabilities = [binom.pmf(j, Constants.N, Constants.p) * 100 for j in range(0, len(TABprices))]
                    elif Constants.dist_initprices == 'uniform':
                        TABprobabilities = [randint.pmf(j, 0, len(TABprices)) * 100 for j in range(1, len(TABprices))] + [100 - sum([randint.pmf(j, 0, len(TABprices)) * 100 for j in range(1, len(TABprices))])]

                else:
                    TABprices = possibleinitprices
                    if Constants.dist_initprices == 'geometric':
                        TABprobabilities = [geom.pmf(j, Constants.p) * 100 for j in range(1, len(TABprices))] + [100 - sum([geom.pmf(j, Constants.p)*100 for j in range(1, len(TABprices))])]
                    elif Constants.dist_initprices == 'poisson':
                        TABprobabilities = [poisson.pmf(j, Constants.p) * 100 for j in range(0, len(TABprices)-1)] + [100 - sum([poisson.pmf(j, Constants.p) * 100 for j in range(0, len(TABprices)-1)])]
                    elif Constants.dist_initprices == 'binomial':
                        TABprobabilities = [binom.pmf(j, Constants.N, Constants.p) * 100 for j in range(0, len(TABprices))] + [100 - sum([binom.pmf(j, Constants.N, Constants.p) * 100 for j in range(0, len(TABprices))])]
                    elif Constants.dist_initprices == 'uniform':
                        TABprobabilities = [randint.pmf(j, 0, len(TABprices)) * 100 for j in range(1, len(TABprices))] + [100 - sum([randint.pmf(j, 0, len(TABprices)) * 100 for j in range(1, len(TABprices))])]

                p.participant.vars['TAB'] = list(zip(TABprices, TABprobabilities))

                # determine control questions
                # ------------------------------------------------------------------------------------------------
                cq_items_binary = [
                    'CQ1',
                    'CQ2',
                    'CQ3',
                    'CQ4',
                    'CQ5',
                    'CQ6',
                    'CQ7',
                    'CQ8',
                    'CQ9'
                ]

                cq_items_fouranswers = [
                    'CQ10',
                    'CQ11',
                    'CQ12',
                    'CQ13',
                    'CQ14',
                    'CQ15',
                    'CQ16'
                ]

                cq_questions_binary = [
                    # --- Questions from Moinas and Pouget (2016) ---
                    _('Is it possible to be first and be proposed to buy at a price of 100,000?'),
                    _('If you are proposed to buy at a price of 1, are you sure to be first?'),
                    _('If you are proposed to buy at a price of 100,000, are you sure to be second?'),
                    _('If you are proposed to buy at a price of 100, are you sure to be third?'),
                    _('If you are proposed to buy at a price of 1,000,000, are you sure to be third ?'),
                    _('If you accept to buy at 100, you will propose to resell at'),
                    _('If you are first, buy at 100, and resell at 1,000, your payoff in the game is'),
                    _('If you are first, buy at 100, and find nobody to resell to at 1,000, your payoff in the game is'),
                    _('If you refuse to buy, your payoff is'),
                ]

                cq_questions_fouranswers = [
                    # --- Questions from Janssen, Füllbrunn, and Weitzel (2018) ---
                    _('What is the probability of being third in line when you have not been offered a price yet?'),
                    _('What is the probability of the first price (P1) being 1,000?'),
                    _('What is the probability of the first price (P1) being 100,000?'),
                    _('If you are offered a price of 1,000, what is the probability of not being last in line?'),
                    _('What is your profit when you are first in line and buy but the person next in line does not buy?'),
                    _('What is your profit if you are second in line and the person before and after you in line buy, but you do not buy?'),
                    _('What is your profit when you are first in line, you decide to buy and the trader next in line also buys?')
                ]

                cq_answers0_binary = [
                                  _('Yes')
                              ] * 5 + [
                                  '1000',
                                  '10 ' + str(Constants.exp_currency),
                                  '0 ' + str(Constants.exp_currency),
                                  '1 ' + str(Constants.exp_currency)
                              ]
                cq_answers1_binary = [
                                  _('No')
                              ] * 5 + [
                                  '1100',
                                  '900 ' + str(Constants.exp_currency),
                                  '10 ' + str(Constants.exp_currency),
                                  '0 ' + str(Constants.exp_currency)
                              ]

                cq_answers0_fouranswers = [
                    '100%',
                    '0%',
                    '0%',
                    '70%',
                    '0 ' + str(Constants.exp_currency),
                    '0 ' + str(Constants.exp_currency),
                    '0 ' + str(Constants.exp_currency)
                ]
                cq_answers1_fouranswers = [
                    '75%',
                    '15%',
                    '15%',
                    '40%',
                    '1 ' + str(Constants.exp_currency),
                    '1 ' + str(Constants.exp_currency),
                    '1 ' + str(Constants.exp_currency)
                ]
                cq_answers2_fouranswers = [
                    '10%',
                    '20%',
                    '20%',
                    '10%',
                    '3 ' + str(Constants.exp_currency),
                    '3 ' + str(Constants.exp_currency),
                    '3 ' + str(Constants.exp_currency)
                ]
                cq_answers3_fouranswers = [
                    '33.33%',
                    '30%',
                    '30%',
                    '30%',
                    '10 ' + str(Constants.exp_currency),
                    '10 ' + str(Constants.exp_currency),
                    '10 ' + str(Constants.exp_currency)
                ]

                cq_correct_binary = [
                    1,
                    0,
                    1,
                    1,
                    0,
                    0,
                    0,
                    0,
                    0
                ]

                cq_correct_fouranswers = [
                    3,
                    2,
                    0,
                    0,
                    0,
                    2,
                    3
                ]

                controlquestions_set1 = [k-1 for k in Constants.controlquestions_set1]
                cq_ids_binary = [k for k in range(1, len(controlquestions_set1)+1)]
                cq_items_binary = [cq_ids_binary[k] for k in controlquestions_set1]
                cq_questions_binary = [cq_questions_binary[k] for k in controlquestions_set1]
                cq_answers0_binary = [cq_answers0_binary[k] for k in controlquestions_set1]
                cq_answers1_binary = [cq_answers1_binary[k] for k in controlquestions_set1]
                cq_correct_binary = [cq_correct_binary[k] for k in controlquestions_set1]
                cq = list(zip(cq_items_binary, cq_ids_binary, cq_questions_binary, cq_answers0_binary, cq_answers1_binary, cq_correct_binary))

                controlquestions_set2 = [k-10 for k in Constants.controlquestions_set2]
                cq_ids_fouranswers = [k for k in range(len(controlquestions_set1) + 1, len(controlquestions_set1) + 1 + len(controlquestions_set2))]
                cq_questions_fouranswers = [cq_questions_fouranswers[k] for k in controlquestions_set2]
                cq_answers0_fouranswers = [cq_answers0_fouranswers[k] for k in controlquestions_set2]
                cq_answers1_fouranswers = [cq_answers1_fouranswers[k] for k in controlquestions_set2]
                cq_answers2_fouranswers = [cq_answers2_fouranswers[k] for k in controlquestions_set2]
                cq_answers3_fouranswers = [cq_answers3_fouranswers[k] for k in controlquestions_set2]
                cq_correct_fouranswers = [cq_correct_fouranswers[k] for k in controlquestions_set2]
                cq1 = list(zip(cq_items_fouranswers, cq_ids_fouranswers, cq_questions_fouranswers, cq_answers0_fouranswers, cq_answers1_fouranswers, cq_answers2_fouranswers, cq_answers3_fouranswers, cq_correct_fouranswers))

                p.participant.vars['cq'] = cq
                p.participant.vars['cq1'] = cq1

                cq_form_fields = ['cq_' + str(k) for k in range(1, len(cq)+1)] + ['cq_' + str(k) for k in range(len(cq) + 1, len(cq) + 1 + len(cq1))]
                p.participant.vars['cq_form_fields'] = cq_form_fields



class Group(BaseGroup):
    pass


class Player(BasePlayer):

    cq1 = models.FloatField()
    cq2 = models.FloatField()
    cq3 = models.FloatField()
    cq4 = models.FloatField()
    cq5 = models.FloatField()

    random_round = models.FloatField()

    consent = models.BooleanField()
    trials = models.IntegerField()

