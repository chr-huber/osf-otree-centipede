## Centipede Game: Experimental Software

---

This repository contains the experimental oTree software used for the centipede game experiments in the manuscript 
'Cooperation among an anonymous group protected Bitcoin during failures of decentralization'.

- The app `ipq_seq` contains the N-player centipede game.
- The app `ipg_seq_2p8r` contains the 2-player centipede game.
- The apps starting with `ins` contain the respective instructions, comprehension questions, and a consent form.

`osf-otree-centipede.otreezip` contains the oTree-zipped software which can be unpacked using the oTree command `otree unzip`.

The software runs on oTree version 3.4.0, using Python 3.8, and is based on the Bubble Game implementation for oTree by Huber, C. (2019). oTree: The bubble game. Journal of Behavioral and Experimental Finance, 22, 3-6.

