from otree.api import Currency as c, currency_range
from ._builtin import Page, WaitPage
from .models import Constants
from settings import *
from django.utils.translation import ugettext as _
import math


class Welcome(Page):

    def is_displayed(self):
        return self.round_number == 1

    form_model = 'player'
    form_fields = ['consent']


class Return(Page):

    def is_displayed(self):
        return self.player.consent is False


# --- Instructions -----------------------------------------------------------------------------------------------------

class Instructions(Page):

    def is_displayed(self):
        return self.round_number == 1

    def vars_for_template(self):

        GTpayoffORIGINAL = self.player.participant.vars['GTpayoffs']
        GTpayoffs = list(zip(*self.player.participant.vars['GTpayoffs'])) + [list(zip(*self.player.participant.vars['GameTree']))[-1]]
        GTpayoffsINTER = GTpayoffs
        GTpayoffs = [tuple(GTpayoffs[j][k] for j in range(0, len(GTpayoffs))) for k in range(0, len(self.player.participant.vars['GameTree']))]

        GTpayoffs_str = list(zip(*GTpayoffs))
        strings = []
        for k in range(0, len(self.player.participant.vars['GameTree'])):
            add = []
            for j in range(0, len(GTpayoffs_str) - 1):
                if GTpayoffs_str[j][k] % 1 == 0:
                    add.extend([int(GTpayoffs_str[j][k])])
                else:
                    add.extend([GTpayoffs_str[j][k]])
            strings.extend([str(tuple(add))])
#        strings = [str(tuple(GTpayoffs_str[j][k] for j in range(0, len(GTpayoffs_str)-1))) for k in range(0, len(self.player.participant.vars['GameTree']))]
        xpoints = self.player.participant.vars['GTxpoints']
        GTpayoffs_str = [(strings[j], xpoints[j]) for j in range(0, len(strings))]

        if Constants.graph_instructions == 'horizontal':
            add_to_last_price = 600/Constants.num_players
            place_of_buy = 300 / Constants.num_players
            font_size_buy = 10 + 4 * (3/Constants.num_players)
            font_size_price = 14+4*(3/Constants.num_players)
            font_size_payoff = 20*(3/Constants.num_players)
        else:
            add_to_last_price = 40+480/Constants.num_prices+40
            place_of_buy = 300/Constants.num_prices+40
            font_size_buy = 12
            font_size_price = 16
            font_size_payoff = 16
        height = add_to_last_price * Constants.num_prices - 90

        return {
            'GameTree': self.player.participant.vars['GameTree'],
            'GTpayoff1': self.player.participant.vars['GTpayoff1'],
            'GTpayoffN': self.player.participant.vars['GTpayoffN'],
            'GTpayoffs': GTpayoffs,
            'GTpayoffs_str': GTpayoffs_str,
            'payoff1_i': self.player.participant.vars['GTpayoff1'][0],
            'payoff2_i': self.player.participant.vars['GTpayoffs'][1][1],
            'payoff2_j': self.player.participant.vars['GTpayoffs'][1][0],
            'payoff3_i': GTpayoffs[2][1],
            'payoff3_j': GTpayoffs[2][0],
            'payoff7_i': GTpayoffs[6][0],
            'payoffn': self.player.participant.vars['GTpayoffN'][0][0][1:3],
            'add_to_last_price': add_to_last_price,
            'place_of_buy': place_of_buy,
            'font_size_buy': font_size_buy,
            'font_size_price': font_size_price,
            'font_size_payoff': font_size_payoff,
            'graph': Constants.graph_instructions,
            'height': height,
            'add_to_last_payoff': add_to_last_price+15,
            'num_players': Constants.num_players,
            'num_playersminus1': Constants.num_players-1,
            'tooltip_price': Constants.tooltip_price,
            'tooltip_payoff': Constants.tooltip_payoff,
            'TAB': self.player.participant.vars['TAB'],
            'exp_currency': Constants.exp_currency,
            'num_repetitions': Constants.num_repetitions,
            'GTpayoffORIGINAL': GTpayoffORIGINAL,
            'GTpayoffINTER': GTpayoffsINTER
        }


class Questions(Page):

    form_model = 'player'
    form_fields = ['cq1', 'cq2', 'cq3', 'cq4', 'cq5', 'trials']

    def is_displayed(self):
        return self.round_number == 1

    def vars_for_template(self):

        GTpayoffs = list(zip(*self.player.participant.vars['GTpayoffs'])) + [list(zip(*self.player.participant.vars['GameTree']))[-1]]
        GTpayoffs = [tuple(GTpayoffs[j][k] for j in range(0, len(GTpayoffs))) for k in range(0, len(self.player.participant.vars['GameTree']))]

        GTpayoffs_str = list(zip(*GTpayoffs))
        strings = []
        for k in range(0, len(self.player.participant.vars['GameTree'])):
            add = []
            for j in range(0, len(GTpayoffs_str) - 1):
                if GTpayoffs_str[j][k] % 1 == 0:
                    add.extend([int(GTpayoffs_str[j][k])])
                else:
                    add.extend([GTpayoffs_str[j][k]])
            strings.extend([str(tuple(add))])
#        strings = [str(tuple(GTpayoffs_str[j][k] for j in range(0, len(GTpayoffs_str)-1))) for k in range(0, len(self.player.participant.vars['GameTree']))]
        xpoints = self.player.participant.vars['GTxpoints']
        GTpayoffs_str = [(strings[j], xpoints[j]) for j in range(0, len(strings))]

        if Constants.graph_instructions == 'horizontal':
            add_to_last_price = 600/Constants.num_players
            place_of_buy = 300 / Constants.num_players
            font_size_buy = 10 + 4 * (3/Constants.num_players)
            font_size_price = 14+4*(3/Constants.num_players)
            font_size_payoff = 20*(3/Constants.num_players)
        else:
            add_to_last_price = 40 + 480 / Constants.num_prices + 40
            place_of_buy = 300 / Constants.num_prices + 40
            font_size_buy = 12
            font_size_price = 16
            font_size_payoff = 16
        height = add_to_last_price * Constants.num_prices - 90

        return {
            'GameTree': self.player.participant.vars['GameTree'],
            'GTpayoff1': self.player.participant.vars['GTpayoff1'],
            'GTpayoffN': self.player.participant.vars['GTpayoffN'],
            'GTpayoffs': GTpayoffs,
            'GTpayoffs_str': GTpayoffs_str,
            'payoff1_i': self.player.participant.vars['GTpayoff1'][0],
            'payoff2_i': self.player.participant.vars['GTpayoffs'][1][1],
            'payoff2_j': self.player.participant.vars['GTpayoffs'][1][0],
            'payoff3_i': GTpayoffs[2][1],
            'payoff3_j': GTpayoffs[2][0],
            'payoff7_i': GTpayoffs[6][0],
            'payoffn': self.player.participant.vars['GTpayoffN'][0][0][1:3],
            'add_to_last_price': add_to_last_price,
            'place_of_buy': place_of_buy,
            'font_size_buy': font_size_buy,
            'font_size_price': font_size_price,
            'font_size_payoff': font_size_payoff,
            'graph': Constants.graph_instructions,
            'height': height,
            'add_to_last_payoff': add_to_last_price+15,
            'num_players': Constants.num_players,
            'num_playersminus1': Constants.num_players-1,
            'tooltip_price': Constants.tooltip_price,
            'tooltip_payoff': Constants.tooltip_payoff,
            'TAB': self.player.participant.vars['TAB'],
            'exp_currency': Constants.exp_currency,
            'num_repetitions': Constants.num_repetitions
        }

# page_sequence = [Welcome] # for testing only
page_sequence = [Welcome, Return, Instructions, Questions]
